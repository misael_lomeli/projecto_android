package com.example.misaellomeli.controlcheckbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText et1, et2;
    private TextView resultado;
    private CheckBox chec1,chec2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et1=(EditText)findViewById(R.id.editText);
        et2=(EditText)findViewById(R.id.editText2);
        resultado=(TextView)findViewById(R.id.textView3);
        chec1=(CheckBox)findViewById(R.id.checkBox);
        chec2=(CheckBox)findViewById(R.id.checkBox2);
    }
    public void operar(View view){
        String valor1=et1.getText().toString();
        String valor2=et2.getText().toString();
        int numero1=Integer.parseInt(valor1);
        int numero2=Integer.parseInt(valor2);
        String resul="";
        if(chec1.isChecked()==true){
            int suma=numero1+numero2;
            resul="La suma es : "+suma;
        }
        if(chec2.isChecked()==true)
        {
            int resta=numero1-numero2;
            resul=resul+"La resta es: "+resta;
        }
        resultado.setText(resul);
    }

}
